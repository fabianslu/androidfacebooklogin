package com.example.fabian.facebooklogin.network;

import com.example.fabian.facebooklogin.models.User;
import com.example.fabian.facebooklogin.models.UserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by fabian on 3/22/17.
 */

public interface ApiInterface {
    @POST("users")
    Call<UserResponse> createUser(@Body User user);
}
