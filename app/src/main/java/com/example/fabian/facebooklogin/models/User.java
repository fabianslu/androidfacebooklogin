
package com.example.fabian.facebooklogin.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("nationalityName")
    @Expose
    private String nationalityName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("roleName")
    @Expose
    private String roleName;
    @SerializedName("addressInfo")
    @Expose
    private List<AddressInfo> addressInfo = null;
    @SerializedName("userContactInfo")
    @Expose
    private List<UserContactInfo> userContactInfo = null;
    @SerializedName("computerName")
    @Expose
    private String computerName;
    @SerializedName("computerUser")
    @Expose
    private String computerUser;
    @SerializedName("activitySourceName")
    @Expose
    private String activitySourceName;
    @SerializedName("computerIp")
    @Expose
    private String computerIp;

    /**
     * No args constructor for use in serialization
     * 
     */
    public User() {
    }

    /**
     * 
     * @param birthday
     * @param activitySourceName
     * @param lastname
     * @param roleName
     * @param password
     * @param username
     * @param email
     * @param computerIp
     * @param name
     * @param addressInfo
     * @param computerName
     * @param computerUser
     * @param nationalityName
     * @param userContactInfo
     */
    public User(String name, String lastname, String password, String birthday, String nationalityName, String email, String username, String roleName, List<AddressInfo> addressInfo, List<UserContactInfo> userContactInfo, String computerName, String computerUser, String activitySourceName, String computerIp) {
        super();
        this.name = name;
        this.lastname = lastname;
        this.password = password;
        this.birthday = birthday;
        this.nationalityName = nationalityName;
        this.email = email;
        this.username = username;
        this.roleName = roleName;
        this.addressInfo = addressInfo;
        this.userContactInfo = userContactInfo;
        this.computerName = computerName;
        this.computerUser = computerUser;
        this.activitySourceName = activitySourceName;
        this.computerIp = computerIp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<AddressInfo> getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(List<AddressInfo> addressInfo) {
        this.addressInfo = addressInfo;
    }

    public List<UserContactInfo> getUserContactInfo() {
        return userContactInfo;
    }

    public void setUserContactInfo(List<UserContactInfo> userContactInfo) {
        this.userContactInfo = userContactInfo;
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public String getComputerUser() {
        return computerUser;
    }

    public void setComputerUser(String computerUser) {
        this.computerUser = computerUser;
    }

    public String getActivitySourceName() {
        return activitySourceName;
    }

    public void setActivitySourceName(String activitySourceName) {
        this.activitySourceName = activitySourceName;
    }

    public String getComputerIp() {
        return computerIp;
    }

    public void setComputerIp(String computerIp) {
        this.computerIp = computerIp;
    }

}
