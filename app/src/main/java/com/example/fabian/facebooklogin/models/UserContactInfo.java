
package com.example.fabian.facebooklogin.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserContactInfo {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserContactInfo() {
    }

    /**
     * 
     * @param name
     * @param value
     */
    public UserContactInfo(String name, String value) {
        super();
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
