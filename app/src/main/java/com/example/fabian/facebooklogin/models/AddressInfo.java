
package com.example.fabian.facebooklogin.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressInfo {

    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitud")
    @Expose
    private String latitud;
    @SerializedName("cityName")
    @Expose
    private String cityName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AddressInfo() {
    }

    /**
     * 
     * @param cityName
     * @param zipCode
     * @param latitud
     * @param address1
     * @param longitude
     * @param address2
     */
    public AddressInfo(String address1, String address2, String zipCode, String longitude, String latitud, String cityName) {
        super();
        this.address1 = address1;
        this.address2 = address2;
        this.zipCode = zipCode;
        this.longitude = longitude;
        this.latitud = latitud;
        this.cityName = cityName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

}
